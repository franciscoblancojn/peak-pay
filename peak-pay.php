<?php
/*
Plugin Name:Peack Pay
Plugin URI: https://gitlab.com/franciscoblancojn/peak-pay
Description: Plugin for Wordpress, used api Peack Pay for generate checkout payment
Author: Francisco Blanco
Version: 1.0.4
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/peak-pay',
	__FILE__,
	'peak-pay'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');


function PEAKPAY_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("PEAKPAY_LOG",true);
define("PEAKPAY_PATH",plugin_dir_path(__FILE__));
define("PEAKPAY_URL",plugin_dir_url(__FILE__));

require_once PEAKPAY_PATH . "src/_index.php";