<?php

require_once PEAKPAY_PATH . "src/img/_index.php";
require_once PEAKPAY_PATH . "src/function/_index.php";
require_once PEAKPAY_PATH . "src/css/_index.php";
require_once PEAKPAY_PATH . "src/js/_index.php";

require_once PEAKPAY_PATH . "src/api/_index.php";
require_once PEAKPAY_PATH . "src/hook/_index.php";
require_once PEAKPAY_PATH . "src/log/_index.php";
require_once PEAKPAY_PATH . "src/page/_index.php";
require_once PEAKPAY_PATH . "src/payment/_index.php";
require_once PEAKPAY_PATH . "src/routes/_index.php";
require_once PEAKPAY_PATH . "src/sass/_index.php";
require_once PEAKPAY_PATH . "src/template/_index.php";
require_once PEAKPAY_PATH . "src/validators/_index.php";