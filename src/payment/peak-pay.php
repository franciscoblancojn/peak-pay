<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'peak_pay_add_gateway_class' );
function peak_pay_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Peak_Pay_Gateway'; // your class name is here
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'peak_pay_init_gateway_class' );
function peak_pay_init_gateway_class() {

	class WC_Peak_Pay_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'peak_pay'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'Peak Pay Gateway';
            $this->method_description = 'Payment for Wordpress use Peak Pay'; // will be displayed on the options page

            $this->notification_url     = PEAKPAY_URL."src/routes/order/processing.php";
            $this->txn_request_url      = get_home_url();
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            $this->txn_request_ip = $this->get_option( 'txn_request_ip' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

 		}
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Peak Pay Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Peak Pay',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via Peak Pay',
                ),
                'configPayment' => array(
                    'title'       => 'Config Payment',
                    'type'        => 'tag',
                    'text'        => "Your config of payment, contact an advisor ",
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
                ),
                'merchant_id' => array(
                    'title'       => 'Merchant Id',
                    'type'        => 'text',
                ),
                'password' => array(
                    'title'       => 'Password',
                    'type'        => 'password',
                ),
            );
	 	}
         /**
          * Custon field tag
          */
        public function generate_tag_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top" class="tag">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <style>
                .tag{
                    background: #1d2327;
                    color: white;
                    box-shadow: -50px 0 #1d2327,50px 0 #1d2327;
                }
                .tag label{
                    color: white !important;
                }
            </style>
            <?php
            return ob_get_clean();
        }
         /**
          * Custon field info
          */
        public function generate_info_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
                // ok, let's display some description before the payment form
                if ( $this->description ) {
                    // you can instructions for test mode, I mean test card numbers etc.
                    if ( $this->testmode ) {
                        $this->description = ' TEST MODE ENABLED.';
                    }
                    // display the description with <p> tags etc.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
                ?>
                <div id="PEAKPAY_content_cc" class="PEAKPAY_content active">
                    <div class="form-row form-row-wide " >
                        <label>
                            Card Number <span class="required">*</span>
                        </label>
                        <div class="PEAKPAY_content_card_number">
                            <input id="PEAKPAY_card_number" name="PEAKPAY_card_number" type="text" autocomplete="off" required placeholder="Card Number">
                            <img src="" alt="" id="PEAKPAY_card_number_img" class="PEAKPAY_card_number_img">
                        </div>
                    </div>
                    <div class="form-row form-row-wide pos-r">
                        <label>
                            Expiry Date (YYYY-MM) <span class="required">*</span>
                        </label>
                        <input id="PEAKPAY_expiry_date" name="PEAKPAY_expiry_date" type="text" autocomplete="off" required placeholder="YYYY-MM">
                        <input id="PEAKPAY_expiry_date_hidden" name="PEAKPAY_expiry_date_hidden" type="month" autocomplete="off" required class="inputDateOver">
                    </div>
                    <div class="form-row form-row-wide">
                        <label>
                            CVV <span class="required">*</span>
                        </label>
                        <input id="PEAKPAY_cvv" name="PEAKPAY_cvv" type="number" autocomplete="off" required max="999" placeholder="XXX" maxlength="3">
                    </div>
                    <div class="clear"></div>
                </div>
                <script>
                    var PEAKPAY_LOG = <?=PEAKPAY_LOG ? "true":"false"?> ;
                    var PEAKPAY_URL = "<?=PEAKPAY_URL?>";
                </script>
                <link rel="stylesheet" href="<?=PEAKPAY_URL?>src/css/peak-pay-checkout.css?v=<?=PEAKPAY_get_version()?>">
                <script src="<?=PEAKPAY_URL?>src/js/peak-pay-checkout.js?v=<?=PEAKPAY_get_version()?>"></script>
                <?php
		}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {
            if(!$this->testmode){
                if( empty( $this->get_option( 'merchant_id' ) )) {
                    wc_add_notice(  '¡Se requiere identificación de comerciante de pago máximo!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'password' ) )) {
                    wc_add_notice(  '¡Se requiere contraseña de pago máximo!', 'error' );
                    return false;
                }
            }
            if( empty( $_POST['PEAKPAY_card_number'] )) {
                wc_add_notice(  '¡Se requiere número de tarjeta!', 'error' );
                return false;
            }
            if( empty( $_POST['PEAKPAY_cvv'] )) {
                wc_add_notice(  '¡Se requiere CVV!', 'error' );
                return false;
            }
            if( empty( $_POST['PEAKPAY_expiry_date'] )) {
                wc_add_notice(  '¡La fecha de caducidad es obligatoria!', 'error' );
                return false;
            }
            if( empty( $_POST['billing_phone'] )) {
                wc_add_notice(  '¡Se requiere teléfono!', 'error' );
                return false;
            }
            if(!preg_match("/[+][0-9]{1,3}[-][0-9]{10}/", $_POST['billing_phone'])){
                wc_add_notice(  '¡El teléfono no es válido! El formato del teléfono es: "Código + Número (10 dígitos)"', 'error' );
                return false;
            }
            return true;
		}
        public function getApi()
        {
            $api = new API_peak_pay(array(
                "merchant_id"   =>  $this->get_option( 'merchant_id' ),
                "password"      =>  $this->get_option( 'password' ),
                "testmode"      =>  $this->testmode,
            ));
            return $api;
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            try {
                global $woocommerce;
    
                $order = wc_get_order( $order_id );
    
                $api = $this->getApi();

                $expiry_date = explode("-",$_POST['PEAKPAY_expiry_date']);

                $expiry_date_month  = $expiry_date[1];
                $expiry_date_year   = substr($expiry_date[0],-2);

                $result = $api->sale(array(
                    'first_name'            => $order->get_billing_first_name(),
                    'last_name'             => $order->get_billing_last_name(),
                    'card_number'           => $_POST['PEAKPAY_card_number'],
                    'expiry_date_month'     => $expiry_date_month,
                    'expiry_date_year'      => $expiry_date_year,
                    'cv2'                   => $_POST['PEAKPAY_cvv'],
                    'amount'                => floatval($order->get_total())*100,
                    'currency_code'         => $order->get_currency(),
                    'email_address'         => $order->get_billing_email(),
                    'phone_number'          => $order->get_billing_phone(),
                    'address1'              => $order->get_billing_address_1(),
                    'address2'              => $order->get_billing_address_2(),
                    'city'                  => $order->get_billing_city(),
                    'province'              => $order->get_billing_state(),
                    'postal_code'           => $order->get_billing_postcode(),
                    'country_code'          => $order->get_billing_country(),
                    'order_id'              => $order->get_id(),
                    'customer_id'           => $order->get_customer_id(),
                    'customer_ip_address'   => $order->get_customer_ip_address(),
                ));
               
                if($result["status_code"] == 0){

                    $order->payment_complete();
                    $order->reduce_order_stock();
                    $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                    $woocommerce->cart->empty_cart();
        
                    update_post_meta(
                        $order_id,
                        "payment",
                        "peak_pay"
                    );
                    update_post_meta(
                        $order_id,
                        "payment_peak_pay",
                        json_encode($result)
                    );
                    addPEAKPAY_LOG(array(
                        "type"      => "sale-success",
                        "data"     => $result,
                    ));
                    return array(
                        'result' => 'success',
                        'redirect' => $order->get_checkout_order_received_url()
                    );	
                }

                addPEAKPAY_LOG(array(
                    "type"      => "sale-error",
                    "error"     => $result,
                ));
                $code = $result["status_code"];
                $msj = PEAKPAY_code_respond($code);

                wc_add_notice( $msj, 'error' );
                return false;

            } catch (Exception $error) {
                addPEAKPAY_LOG(array(
                    "type"      => "sale-error",
                    "error"     => $error,
                ));
                wc_add_notice(  $error->getMessage(), 'error' );
                return false;
            }
	 	}
 	}
}