<?php

function PEAKPAY_get_optionPage($id = "PEAKPAY_optionPage")
{
    $PEAKPAY_optionPage = get_option($id);
    if($PEAKPAY_optionPage === false || $PEAKPAY_optionPage == null || $PEAKPAY_optionPage == ""){
        $PEAKPAY_optionPage = "[]";
    }
    $PEAKPAY_optionPage = json_decode($PEAKPAY_optionPage,true);
    return $PEAKPAY_optionPage;
}

function PEAKPAY_put_optionPage($id,$newItem)
{
    $PEAKPAY_optionPage = PEAKPAY_get_optionPage($id);
    $PEAKPAY_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($PEAKPAY_optionPage));
}