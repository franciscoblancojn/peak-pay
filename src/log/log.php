<?php
if(PEAKPAY_LOG){
    function add_PEAKPAY_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'PEAKPAY_LOG',
                'title' => 'PEAKPAY_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=PEAKPAY_LOG'
            )
        );
    }

    function PEAKPAY_LOG_option_page()
    {
        add_options_page(
            'Log OAcquiring Tech',
            'Log OAcquiring Tech',
            'manage_options',
            'PEAKPAY_LOG',
            'PEAKPAY_LOG_page'
        );
    }

    function PEAKPAY_LOG_page()
    {
        $log = PEAKPAY_get_optionPage("PEAKPAY_LOG");
        $log = array_reverse($log);
        ?>
        <script>
            const log = <?=json_encode($log)?>;
        </script>
        <h1>
            Log
        </h1>
        <pre><?php var_dump($log);?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_PEAKPAY_LOG_option_page', 100);

    add_action('admin_menu', 'PEAKPAY_LOG_option_page');

}

function addPEAKPAY_LOG($newLog)
{
    if(!PEAKPAY_LOG){
        return;
    }
    PEAKPAY_put_optionPage("PEAKPAY_LOG",$newLog);
}