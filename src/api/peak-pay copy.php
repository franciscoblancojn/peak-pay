<?php


class API_peak_pay
{
    private $URL_PRO            = "https://api.secure2tech.eu";

    private $URL                = "";
    private $website_id         = "";
    private $password           = "";

    private $URL_DEV            = "https://api.secure2tech.eu";
    private $website_id_DEV     = "testing";
    private $password_DEV       = "6G2376rJ";

    function __construct($atts = array())
    {
        $this->website_id   = $atts["website_id"];
        $this->password     = $atts["password"];
        $this->URL          = $this->URL_PRO;

        if($atts["testmode"]){
            $this->website_id   = $this->website_id_DEV;
            $this->password     = $this->password_DEV;
            $this->URL          = $this->URL_DEV;
        }
    }
    private function request($atts = array())
    {
        $curl = curl_init();
        $config =  array(
            //SOLO esta api
                CURLOPT_USERAGENT       => 'example',
                CURLOPT_CONNECTTIMEOUT  => 60,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_SSL_VERIFYHOST  => false,

            CURLOPT_URL             => $this->URL.$atts["url"],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => 1,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => $atts["method"],
            CURLOPT_POSTFIELDS      => http_build_query($atts["data"]),
            CURLOPT_HTTPHEADER      => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: '.$this->TOKEN,
            ),
        );
        curl_setopt_array($curl,$config);

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response,true);

        $atts["header"] = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization '.$this->TOKEN,
        );
        addPEAKPAY_LOG(array(
            "type"      => "REQUEST",
            "config"    => $atts,
            "result"    => $response,
        ));

        return $response;
    }
    public function sale($atts = array())
    {
        $data = array(
            'website_id'            => $this->website_id,
            'password'              => $this->password,

            'first_name'            => $atts['first_name'],
            'last_name'             => $atts['last_name'],
            'card_number'           => $atts['card_number'],
            'expiry_date_month'     => $atts['expiry_date_month'],
            'expiry_date_year'      => $atts['expiry_date_year'],
            'cv2'                   => $atts['cv2'],
            'amount'                => $atts['amount'],
            'currency_code'         => $atts['currency_code'],
            'email_address'         => $atts['email_address'],
            'phone_number'          => $atts['phone_number'],
            'address1'              => $atts['address1'],
            'address2'              => $atts['address2'],
            'city'                  => $atts['city'],
            'province'              => $atts['province'],
            'postal_code'           => $atts['postal_code'],
            'country_code'          => $atts['country_code'],
            'order_id'              => $atts['order_id'],
            'customer_id'           => $atts['customer_id'],
            'customer_ip_address'   => $atts['customer_ip_address'],
        );
        addPEAKPAY_LOG(array(
            "type"      => "pre sale",
            "data"      => $data,
        ));
        return $this->request(array(
            "method"    => "POST",
            "url"       => "/sale-rest",
            "data"      => $data
        ));
    }
}