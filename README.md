# Peack Pay

Plugin for Wordpress, used api Peack Pay for generate checkout payment

#### Version: 1.0.1

## Testing

#### Config

Test mode : true
![alt testingMode](https://gitlab.com/franciscoblancojn/peak-pay/-/raw/master/doc/img/testingMode.png)


#### Checkout

```json
{
    "card_number" : 4597723679422665,
    "expiry_date" : "any",
    "cvv" : "any"
}
```

![alt testingCheckout](https://gitlab.com/franciscoblancojn/peak-pay/-/raw/master/doc/img/testingCheckout.png)